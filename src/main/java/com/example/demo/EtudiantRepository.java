package com.example.demo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EtudiantRepository 
	extends JpaRepository<Etudiant, Long>{
	@Query("select p from Etudiant e where e.nom like :x")
	public List<Etudiant> chercher(@Param("x") String nom);

}
